# Basic Linux Docker Images

This project contains definitions for devcontainer docker images for projects not needing special tools.

## Available Tags

- `22.04`, `latest`

## Available Platforms

All images are available for the following platforms:

* linux/amd64

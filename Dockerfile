#-------------------------------------------------------------------------------------------------------------
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License. See https://go.microsoft.com/fwlink/?linkid=2090316 for license information.
#-------------------------------------------------------------------------------------------------------------
ARG BASE_VARIANT

FROM --platform=$TARGETPLATFORM mcr.microsoft.com/vscode/devcontainers/base:${BASE_VARIANT}

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Prepare directory for GnuPG so that the permissions are set correctly.
RUN install -d -m 0700 -o $USER_UID -g $USER_GID /home/$USERNAME/.gnupg
